/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo;

import com.sc.opencv.demo.ui.FlowPanel;
import org.opencv.core.Mat;

/**
 *
 * @author lucifer
 */
public class InputCVMatrix extends ProcessCVMatrix {

    public InputCVMatrix(FlowPanel panel) {
        super(panel);
    }

    @Override
    protected Mat processMatrix(Mat matrix) {
        return matrix.clone();
    }

}
