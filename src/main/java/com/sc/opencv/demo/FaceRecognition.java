/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo;

import com.sc.opencv.demo.ui.FlowPanel;
import com.sc.opencv.demo.vio.CamReader;
import com.sc.opencv.demo.vio.VideoReader;
import com.sc.opencv.demo.vio.VideoReaderPool;
import java.awt.Dimension;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_java;
import org.bytedeco.javacv.FrameGrabber;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.objdetect.CascadeClassifier;

/**
 *
 * @author lucifer
 */
public class FaceRecognition {

    static JFrame frame = new JFrame();
    static FlowPanel fp = new FlowPanel();
    static JScrollPane sp = new JScrollPane(fp);

    public static void main(String[] args) throws FrameGrabber.Exception, InterruptedException {
        // /home/lucifer/Изображения/qq/P70913-123707.jpg

        Loader.load(opencv_java.class);

        frame.getContentPane().add(sp);

        VideoReaderPool vrp = new VideoReaderPool();
        startVideo(vrp, 0);

//        Mat image = Imgcodecs.imread("/home/lucifer/Изображения/1.png");
//        createMatrixPipe().process(image);
        frame.setMinimumSize(new Dimension(640, 480));
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private static ProcessCVMatrix createMatrixPipe() {
        Mat open = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Mat close = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Mat dilate = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5));

        CascadeClassifier cc = new CascadeClassifier("haar.xml");

        final InputCVMatrix iv = new InputCVMatrix(null);

        final int scale = 2;

        iv.setNext(new ProcessCVMatrix(null) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.GaussianBlur(out, out, new Size(5, 5), 5);
                Imgproc.resize(matrix, out, new Size(matrix.cols() / scale, matrix.rows() / scale));
                setEnv("original", matrix);
                setEnv("scale", scale);
                return out;
            }
        }).setNext(new ProcessCVMatrix(null) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_DILATE, dilate);

                return out;
            }
        }).setNext(new ProcessCVMatrix(null) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_ERODE, erode);

                return out;
            }
        }).setNext(new ProcessCVMatrix(null) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_OPEN, open);
                return out;
            }
        }).setNext(new ProcessCVMatrix(null) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_CLOSE, close);
                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                MatOfRect mr = new MatOfRect();
                Mat bw = matrix.clone();
                Imgproc.cvtColor(matrix, bw, Imgproc.COLOR_BGR2GRAY);
                cc.detectMultiScale(bw, mr, 1.1, 7);

                Mat out = ((Mat) getEnv("original")).clone();

                for (Rect r : mr.toList()) {
                    r = explodeRect(r, 2, 15);
                    r = mulRect(r, getEnv("scale"));
                    Imgproc.rectangle(out, r, new Scalar(0, 255, 0));
                }

                return out;
            }

            private Rect explodeRect(Rect r, int i) {
                return new Rect(r.x - i, r.y - i, r.width + i * 2, r.height + i * 2);
            }

            private Rect explodeRect(Rect r, int i, int i0) {
                return new Rect(r.x - i, r.y - i0, r.width + i * 2, r.height + i0 * 2);
            }

            private Rect mulRect(Rect r, int i) {
                return new Rect(r.x * i, r.y * i, r.width * i, r.height * i);
            }
        });
        return iv;
    }

    private static void startVideo(VideoReaderPool vrp, String file) throws FrameGrabber.Exception {
        VideoReader vr = vrp.startVideoReader(file);
        ProcessCVMatrix iv = createMatrixPipe();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    long start = System.currentTimeMillis();
                    iv.process(vr.getMat());
                    long delta = System.currentTimeMillis() - start;
                    double fps = 1000.0 / vr.getFPS() - delta;
                    if (fps > 0) {
                        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos((long) fps));
                    }
                }
            }
        }).start();

    }

    private static void startVideo(VideoReaderPool vrp, int device) throws FrameGrabber.Exception {
        CamReader vr = vrp.startVideoReader(device);
        ProcessCVMatrix iv = createMatrixPipe();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    long start = System.currentTimeMillis();
                    iv.process(vr.getMat());
                    long delta = System.currentTimeMillis() - start;
                    double fps = 1000.0 / vr.getFPS() - delta;
                    if (fps > 0) {
                        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos((long) fps));
                    }
                }
            }
        }).start();

    }
}
