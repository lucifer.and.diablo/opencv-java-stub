/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo;

import com.sc.opencv.demo.ui.FlowPanel;
import com.sc.opencv.demo.ui.ImageComponent;
import java.util.HashMap;
import java.util.Map;
import org.opencv.core.Mat;

/**
 *
 * @author lucifer
 */
public abstract class ProcessCVMatrix {

    private ProcessCVMatrix next;
    private ProcessCVMatrix prev;
    private final FlowPanel panel;
    private ImageComponent ic;
    private Map<String, Object> env = new HashMap<>();

    public ProcessCVMatrix() {
        this.panel = null;
    }

    public ProcessCVMatrix(FlowPanel panel) {
        this.panel = panel;
    }

    public <T> T getEnv(String key) {
        if (env.containsKey(key)) {
            return (T) env.get(key);
        }
        if (prev != null) {
            return prev.getEnv(key);
        }
        return null;
    }

    public <T> void setEnv(String key, T obj) {
        env.put(key, obj);
    }

    protected abstract Mat processMatrix(Mat matrix);

    public ProcessCVMatrix setNext(ProcessCVMatrix next) {
        this.next = next;
        this.next.prev = this;
        return next;
    }

    public ProcessCVMatrix getNext() {
        return next;
    }

    public Mat process(Mat source) {
        try {
            if (source.empty()) {
                return source;
            }
            if (panel != null) {
                if (ic == null && source.cols() != 0 && source.rows() != 0) {
                    ic = new ImageComponent(source.cols(), source.rows());
                    panel.add(ic);
                    panel.updateUI();
                }
            }

            Mat out = processMatrix(source);
            if (ic != null) {
                if (ic.getSize().width != out.cols() || ic.getSize().height != out.rows()) {
                    ic.setSize(out.cols(), out.rows());
                }
                ic.drawScaledMat(out);
            }

            if (next != null) {
                out = next.process(out);
            }

            return out;
        } catch (Throwable t) {
            t.printStackTrace();
            throw new RuntimeException(t);
        }
    }

}
