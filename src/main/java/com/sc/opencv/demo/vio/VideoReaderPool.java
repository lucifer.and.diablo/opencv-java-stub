/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo.vio;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.bytedeco.javacv.FrameGrabber;

/**
 *
 * @author lucifer
 */
public class VideoReaderPool {

    private ExecutorService threadPool = Executors.newCachedThreadPool();
    private List<Reader> videoReaders = new ArrayList<>();

    public VideoReaderPool() {
    }

    public VideoReader startVideoReader(String file) throws FrameGrabber.Exception {
        VideoReader vr = new VideoReader(file);
        vr.setPool(this);
        videoReaders.add(vr);
        threadPool.submit(vr);
        return vr;
    }

    public CamReader startVideoReader(int device) throws FrameGrabber.Exception {
        CamReader vr = new CamReader(device);
        vr.setPool(this);
        videoReaders.add(vr);
        threadPool.submit(vr);
        return vr;
    }

    void finish(Reader aThis) {
        videoReaders.remove(aThis);
    }

}
