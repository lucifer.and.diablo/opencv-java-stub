/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo.vio;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import org.bytedeco.javacv.FFmpegFrameGrabber;
import org.bytedeco.javacv.Frame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameConverter;
import org.opencv.core.Mat;

/**
 *
 * @author lucifer
 */
public class VideoReader extends Reader implements Runnable {

    private final String file;
    private Thread thread;
    private FFmpegFrameGrabber videoCapture;
    private boolean started;
    private VideoReaderPool pool;
    private final Mat mat;
    private long mSystemVideoClockStartTime;
    private long mFirstVideoTimestampInStream;
    private OpenCVFrameConverter.ToOrgOpenCvCoreMat converter = new OpenCVFrameConverter.ToOrgOpenCvCoreMat();

    public VideoReader(String file) {
        this.file = file;
        this.started = true;
        this.mat = new Mat();
    }

    @Override
    public void run() {
        setThread(Thread.currentThread());
        try {
            initVideoInput();
        } catch (FrameGrabber.Exception ex) {
            ex.printStackTrace();

            return;
        }
        while (started) {
            try {
                Frame frame = videoCapture.grabFrame(false, true, true, false);
                converter.convert(frame).copyTo(mat);
            } catch (FrameGrabber.Exception ex) {
                ex.printStackTrace();
                break;
            }
            long delay = millisecondsUntilTimeToDisplay();
            if (delay > 0) {
                LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos(delay));
            }
        }
        pool.finish(this);
    }

    public void setThread(Thread thread) {
        this.thread = thread;
    }

    public Thread getThread() {
        return thread;
    }

    private void initVideoInput() throws FrameGrabber.Exception {
        videoCapture = new FFmpegFrameGrabber(file);
        videoCapture.start();
    }

    public void setPool(VideoReaderPool pool) {
        this.pool = pool;
    }

    public VideoReaderPool getPool() {
        return pool;
    }

    public Mat getMat() {
        return mat;
    }

    private long millisecondsUntilTimeToDisplay() {
        long millisecondsToSleep = 0;
        if (mFirstVideoTimestampInStream == 0L) {
            mFirstVideoTimestampInStream = videoCapture.getTimestamp();
            mSystemVideoClockStartTime = System.currentTimeMillis();
            millisecondsToSleep = 0;
        } else {
            long systemClockCurrentTime = System.currentTimeMillis();
            long millisecondsClockTimeSinceStartofVideo = (systemClockCurrentTime - mSystemVideoClockStartTime) * 1000;
            long millisecondsStreamTimeSinceStartOfVideo = ((long) videoCapture.getTimestamp() - mFirstVideoTimestampInStream);
            final long millisecondsTolerance = 0;
            millisecondsToSleep = (millisecondsStreamTimeSinceStartOfVideo - (millisecondsClockTimeSinceStartofVideo + millisecondsTolerance));
        }

        return millisecondsToSleep / 1000;
    }

    public double getFPS() {
        return videoCapture == null ? 1 : videoCapture.getFrameRate();
    }
}
