/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo;

import com.sc.opencv.demo.ui.FlowPanel;
import com.sc.opencv.demo.vio.CamReader;
import com.sc.opencv.demo.vio.VideoReaderPool;
import java.awt.Dimension;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.LockSupport;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_java;
import org.bytedeco.javacv.FrameGrabber;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.video.BackgroundSubtractor;
import org.opencv.video.BackgroundSubtractorKNN;
import org.opencv.video.BackgroundSubtractorMOG2;
import org.opencv.video.Video;

/**
 *
 * @author lucifer
 */
public class BackgroundSubstraction {

    static JFrame frame = new JFrame();
    static FlowPanel fp = new FlowPanel();
    static JScrollPane sp = new JScrollPane(fp);

    public static void main(String[] args) throws FrameGrabber.Exception {
        Loader.load(opencv_java.class);

        frame.getContentPane().add(sp);
        frame.setMinimumSize(new Dimension(640, 480));
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        VideoReaderPool vrp = new VideoReaderPool();
        startVideo(vrp, 0);
    }

    private static ProcessCVMatrix createMatrixPipe() {

        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Mat dilate = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));

        final InputCVMatrix iv = new InputCVMatrix(fp);

        BackgroundSubtractor bs = Video.createBackgroundSubtractorKNN();

        iv.setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat bg = new Mat();
                int learn = getEnv("learn");
                if (learn > 0) {
                    learn--;
                    setEnv("learn", learn);
                }
                bs.apply(matrix, bg, learn > 0 ? 0.1 : 0.0000001);

                Imgproc.morphologyEx(bg, bg, Imgproc.MORPH_ERODE, erode);
                Imgproc.morphologyEx(bg, bg, Imgproc.MORPH_DILATE, dilate);

                Imgproc.cvtColor(bg, bg, Imgproc.COLOR_GRAY2BGR);

                Mat out = new Mat();
                Core.bitwise_and(matrix, bg, out);

                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat t = new Mat();
                Imgproc.Canny(matrix, t, 100, 200);
                return t;
            }
        });
        iv.setEnv("learn", 400);
        return iv;
    }

    private static void startVideo(VideoReaderPool vrp, int device) throws FrameGrabber.Exception {

        CamReader vr = vrp.startVideoReader(device);
        ProcessCVMatrix iv = createMatrixPipe();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    long start = System.currentTimeMillis();
                    iv.process(vr.getMat());
                    long delta = System.currentTimeMillis() - start;
                    double fps = 1000.0 / vr.getFPS() - delta;
                    if (fps > 0) {
                        LockSupport.parkNanos(TimeUnit.MILLISECONDS.toNanos((long) fps));
                    }

                }
            }
        }).start();

    }
}
