/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo.ui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.concurrent.locks.ReentrantLock;
import javax.imageio.ImageIO;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

/**
 *
 * @author Lucifer
 */
public class ImageComponent extends JPanel {

    private static final long serialVersionUID = 2779938619562855754L;

    private final Dimension size;
    private volatile BufferedImage image;

    private final ReentrantLock lock = new ReentrantLock();

    public BufferedImage getImage() {
        return image;
    }

    public void drawScaledMat(Mat mat) {
        MatOfByte mob = new MatOfByte();
        Imgcodecs.imencode(".bmp", mat, mob);
        try {
            InputStream in = new ByteArrayInputStream(mob.toArray());
            mob.release();
            drawScaledImage(ImageIO.read(in));
        } catch (Exception e) {

        }
    }

    public void drawScaledImage(BufferedImage img) {
        if (image == null) {
            return;
        }

        lock.lock();
        try {
            Graphics2D g = (Graphics2D) image.getGraphics();

            double dx = (double) img.getHeight() / (double) img.getWidth();

            double w = image.getWidth();
            double h = image.getWidth() * dx;

            if (h > image.getHeight()) {
                h = image.getHeight();
                w = image.getHeight() / dx;
            }

            double x = (image.getWidth() - w) / 2.0;
            double y = (image.getHeight() - h) / 2.0;

            g.drawImage(img, (int) x, (int) y, (int) (x + w), (int) (y + h), 0, 0, img.getWidth(), img.getHeight(), null);
        } finally {
            lock.unlock();
        }

        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                repaint();
            }
        });
    }

    public ImageComponent(int width, int height) {
        size = new Dimension(width, height);
        image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

    }

    @Override
    protected void paintComponent(Graphics g) {
        lock.lock();
        try {
            g.drawImage(image, 0, 0, null);
        } finally {
            lock.unlock();
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return size;
    }

    @Override
    public void setSize(int width, int height) {
        Dimension d = new Dimension(width, height);
        size.setSize(d);
        lock.lock();
        try {
            if (image.getWidth() != width || image.getHeight() != height) {
                if (image != null) {
                    image.flush();
                }
                image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
            }

        } finally {
            lock.unlock();
        }
        super.setSize(width, height);
    }

    @Override
    public void setSize(Dimension d) {
        setSize(d.width, d.height);
    }

    @Override
    public Dimension getSize() {
        return size;
    }

    public Graphics2D getImageGraphics() {
        return (Graphics2D) image.getGraphics();
    }

    public void updateImage() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                repaint();
            }
        });
    }
}
