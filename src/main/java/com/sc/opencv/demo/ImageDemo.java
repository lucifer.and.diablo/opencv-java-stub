/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sc.opencv.demo;

import com.sc.opencv.demo.ui.FlowPanel;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import org.bytedeco.javacpp.Loader;
import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_java;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/**
 *
 * @author lucifer
 */
public class ImageDemo {

    static JFrame frame = new JFrame();
    static FlowPanel fp = new FlowPanel();
    static JScrollPane sp = new JScrollPane(fp);

    public static void main(String[] args) {
        Loader.load(opencv_java.class);

        frame.getContentPane().add(sp);
        frame.setMinimumSize(new Dimension(640, 480));
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Mat image = Imgcodecs.imread("./demo.jpg");
        createMatrixPipe().process(createMatrixPipe().process(image));
    }

    private static ProcessCVMatrix createMatrixPipe() {
        Mat open = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
        Mat close = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
        Mat erode = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));
        Mat dilate = Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(3, 3));

        final InputCVMatrix iv = new InputCVMatrix(fp);

        iv.setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                if (matrix.type() >> opencv_core.CV_CN_SHIFT == 3) {
                    Imgproc.cvtColor(matrix, matrix, Imgproc.COLOR_BGR2GRAY);
                }
                setEnv("original", matrix);
                Mat out = matrix.clone();
                Imgproc.GaussianBlur(out, out, new Size(7, 7), 3);
                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {

                Mat out = matrix.clone();

                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_DILATE, dilate);

                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_ERODE, erode);

                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_OPEN, open);
                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.morphologyEx(matrix, out, Imgproc.MORPH_CLOSE, close);
                return out;
            }
        }).setNext(new ProcessCVMatrix(fp) {
            @Override
            protected Mat processMatrix(Mat matrix) {
                Mat out = matrix.clone();
                Imgproc.GaussianBlur(out, out, new Size(5, 5), 3);
                Imgproc.threshold(out, out, 160, 255, Imgproc.THRESH_BINARY_INV);
                Core.add(getEnv("original"), out, out);
                Core.bitwise_not(out, out);
                Core.add(getEnv("original"), out, out);
                return out;
            }
        });

        return iv;
    }
}
